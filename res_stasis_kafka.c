/**
 * res_stasis_kafka
 *
 * Copyright (C) 2019
 *
 * Dmitry Wagin <dmitry.wagin@ya.ru>
 */

#include <asterisk.h>

#include <asterisk/cli.h>
#include <asterisk/config.h>
#include <asterisk/json.h>
#include <asterisk/module.h>
#include <asterisk/parking.h>
#include <asterisk/paths.h>     /* use ast_config_AST_SYSTEM_NAME */
#include <asterisk/pickup.h>
#include <asterisk/rtp_engine.h>
#include <asterisk/stasis_bridges.h>
#include <asterisk/stasis_channels.h>
#include <asterisk/stasis_message_router.h>
#include <asterisk/taskprocessor.h>

#include <strings.h>

#include <librdkafka/rdkafka.h>

static const char *config_file = "res_stasis_kafka.conf";

/*! Stasis message router */
static struct stasis_message_router *stasis_router;

/*! Aggregation topic for all topics */
static struct stasis_topic *aggregation_topic;

/*! Subscription for forwarding the channel caching topic */
static struct stasis_forward *channel_forwarder;

/*! Subscription for forwarding the channel caching topic */
static struct stasis_forward *bridge_forwarder;

/*! Subscription for forwarding the parking topic */
static struct stasis_forward *parking_forwarder;

/*! Subscription for forwarding the parking topic */
static struct stasis_forward *rtp_forwarder;

struct kafka_producer {
	char name[80];
	char *topic;
	rd_kafka_t *rk;
};

static struct ao2_container *kafka_producer_container;

static void produce(const char *key, size_t key_len, const char *payload, size_t payload_len)
{
	struct ao2_iterator aoi;
	struct kafka_producer *producer;
	rd_kafka_resp_err_t err;

	ast_debug(5, "Key=%s, Value=%s\n", key, payload);

	aoi = ao2_iterator_init(kafka_producer_container, 0);
	while ((producer = ao2_iterator_next(&aoi))) {
		rd_kafka_poll(producer->rk, 0); /* non-blocking */
		err = rd_kafka_producev(
		    producer->rk,
		    RD_KAFKA_V_TOPIC(producer->topic),
		    RD_KAFKA_V_KEY(key, key_len),
		    RD_KAFKA_V_VALUE((void *) payload, payload_len),
		    RD_KAFKA_V_MSGFLAGS(RD_KAFKA_MSG_F_COPY),
		    RD_KAFKA_V_END
		);

		if (err) {
			char err_str[512];
			ast_log(LOG_ERROR, "[%s] Failed to produce: %s\n", producer->name, rd_kafka_err2str(err));
			err = rd_kafka_fatal_error(producer->rk, err_str, sizeof(err_str));
			if (err) {
				ast_log(LOG_ERROR, "[%s] FATAL ERROR: %s: %s\n", producer->name,
				        rd_kafka_err2str(err), err_str);
			}
		}

		ao2_ref(producer, -1);
	}
	ao2_iterator_destroy(&aoi);
}

static void produce_json(struct ast_json *json)
{
	const char *key;
	char *payload;
	size_t key_len, payload_len;

	payload = ast_json_dump_string_format(json, AST_JSON_COMPACT);
	ast_json_unref(json);
	if (!payload) {
		return;
	}
	payload_len = strlen(payload);

	key = ast_config_AST_SYSTEM_NAME ?: " ";
	key_len = strlen(key);

	produce(key, key_len, payload, payload_len);

	ast_json_free(payload);
}

static char *handle_cli_produce(struct ast_cli_entry *e, int cmd, struct ast_cli_args *a)
{
	const char *key;
	const char *payload;
	size_t key_len, payload_len;

	switch (cmd) {
	case CLI_INIT:
		e->command = "kafka produce";
		e->usage =
		    "Usage: kafka produce <message>\n"
		    "       Produce message to Kafka.\n";
		return NULL;
	case CLI_GENERATE:
		return NULL;
	}

	if (a->argc < 3) {
		return CLI_SHOWUSAGE;
	}

	key = ast_config_AST_SYSTEM_NAME ?: " ";
	key_len = strlen(key);

	payload = a->argv[2];
	payload_len = strlen(payload);

	produce(key, key_len, payload, payload_len);

	return CLI_SUCCESS;
}

static struct ast_json *channel_state_change(
    struct ast_channel_snapshot *old_snapshot,
    struct ast_channel_snapshot *new_snapshot,
    const struct timeval *event_time)
{
	int is_hungup, was_hungup;

	if (!old_snapshot) {
		return ast_json_pack("{s: s, s: o, s: o}",
		                     "type", "ChannelNew",
		                     "timestamp", ast_json_timeval(*event_time, NULL),
		                     "channel", ast_channel_snapshot_to_json(new_snapshot, NULL));
	}

	was_hungup = ast_test_flag(&old_snapshot->flags, AST_FLAG_DEAD) ? 1 : 0;
	is_hungup = ast_test_flag(&new_snapshot->flags, AST_FLAG_DEAD) ? 1 : 0;

	if (!was_hungup && is_hungup) {
		return ast_json_pack("{s: s, s: o, s: i, s: s, s: o}",
		                     "type", "ChannelHangup",
		                     "timestamp", ast_json_timeval(*event_time, NULL),
		                     "hangupcause", new_snapshot->hangup->cause,
		                     "hangupsource", new_snapshot->hangup->source,
		                     "channel", ast_channel_snapshot_to_json(new_snapshot, NULL));
	}

	if (old_snapshot->state != new_snapshot->state) {
		return ast_json_pack("{s: s, s: o, s: o}",
		                     "type", "ChannelState",
		                     "timestamp", ast_json_timeval(*event_time, NULL),
		                     "channel", ast_channel_snapshot_to_json(new_snapshot, NULL));
	}

	return NULL;
}

static struct ast_json *channel_new_accountcode(
    struct ast_channel_snapshot *old_snapshot,
    struct ast_channel_snapshot *new_snapshot,
    const struct timeval *event_time)
{
	if (old_snapshot && strcmp(old_snapshot->base->accountcode, new_snapshot->base->accountcode)) {
		return ast_json_pack("{s: s, s: o, s: o}",
		                     "type", "ChannelAccountcode",
		                     "timestamp", ast_json_timeval(*event_time, NULL),
		                     "channel", ast_channel_snapshot_to_json(new_snapshot, NULL));
	}

	return NULL;
}

typedef struct ast_json *(*channel_snapshot_monitor)(
    struct ast_channel_snapshot *old_snapshot,
    struct ast_channel_snapshot *new_snapshot,
    const struct timeval *event_time);

channel_snapshot_monitor channel_monitors[] = {
    channel_state_change,
    channel_new_accountcode
};

static int filter_channel_snapshot(struct ast_channel_snapshot *snapshot)
{
	if (!snapshot) {
		return 0;
	}
	return snapshot->base->tech_properties & AST_CHAN_TP_INTERNAL;
}

static void channel_snapshot_update_cb(void *data, struct stasis_subscription *sub, struct stasis_message *message)
{
	struct ast_channel_snapshot_update *update = stasis_message_data(message);
	struct ast_json *json;
	size_t i;

	if (!update->new_snapshot ||
	    filter_channel_snapshot(update->old_snapshot) ||
	    filter_channel_snapshot(update->new_snapshot)) {
		return;
	}

	for (i = 0; i < ARRAY_LEN(channel_monitors); ++i) {
		json = channel_monitors[i](update->old_snapshot, update->new_snapshot,
		                           stasis_message_timestamp(message));
		if (json) {
			produce_json(json);
		}
	}
}

static void channel_monitor_start_cb(void *data, struct stasis_subscription *sub, struct stasis_message *message)
{
	struct ast_channel_blob *payload = stasis_message_data(message);
	struct ast_channel_snapshot *snapshot = payload->snapshot;
	const struct timeval *event_time = stasis_message_timestamp(message);
	struct ast_json *json_channel;
	struct ast_json *json;

	json_channel = ast_channel_snapshot_to_json(snapshot, NULL);
	if (!json_channel) {
		return;
	}

	json = ast_json_pack("{s: s, s: o, s: o}",
	                     "type", "MonitorStart",
	                     "timestamp", ast_json_timeval(*event_time, NULL),
	                     "channel", json_channel);
	if (!json) {
		ast_log(LOG_ERROR, "Failed to pack JSON message\n");
		return;
	}
	produce_json(json);
}

static void channel_monitor_stop_cb(void *data, struct stasis_subscription *sub, struct stasis_message *message)
{
	struct ast_channel_blob *payload = stasis_message_data(message);
	struct ast_channel_snapshot *snapshot = payload->snapshot;
	const struct timeval *event_time = stasis_message_timestamp(message);
	struct ast_json *json_channel;
	struct ast_json *json;

	json_channel = ast_channel_snapshot_to_json(snapshot, NULL);
	if (!json_channel) {
		return;
	}

	json = ast_json_pack("{s: s, s: o, s: o}",
	                     "type", "MonitorStop",
	                     "timestamp", ast_json_timeval(*event_time, NULL),
	                     "channel", json_channel);
	if (!json) {
		ast_log(LOG_ERROR, "Failed to pack JSON message\n");
		return;
	}
	produce_json(json);
}

static void ignore_cb(void *data, struct stasis_subscription *sub, struct stasis_message *message)
{
	return;
}

static void default_cb(void *data, struct stasis_subscription *sub, struct stasis_message *message)
{
	struct ast_json *json = stasis_message_to_json(message, NULL);
	if (!json) {
		return;
	}
	produce_json(json);
}

static void rk_logger(const rd_kafka_t *rk, int level, const char *fac, const char *buf)
{
	/* see syslog.h and asterisk/logger.h */
	switch (level) {
	case 0: /* LOG_EMERG */
	case 1: /* LOG_ALERT */
	case 2: /* LOG_CRIT */
	case 3: /* LOG_ERR */
		ast_log(AST_LOG_ERROR, "RDKAFKA-%i-%s: %s: %s\n", level, fac, rk ? rd_kafka_name(rk) : NULL, buf);
		break;
	case 4: /* LOG_WARNING */
		ast_log(AST_LOG_WARNING, "RDKAFKA-%i-%s: %s: %s\n", level, fac, rk ? rd_kafka_name(rk) : NULL, buf);
		break;
	case 5: /* LOG_NOTICE */
		ast_log(AST_LOG_NOTICE, "RDKAFKA-%i-%s: %s: %s\n", level, fac, rk ? rd_kafka_name(rk) : NULL, buf);
		break;
	case 6: /* LOG_INFO */
		ast_log(AST_LOG_VERBOSE, "RDKAFKA-%i-%s: %s: %s\n", level, fac, rk ? rd_kafka_name(rk) : NULL, buf);
		break;
	default: /* AST_LOG_DEBUG */
		ast_log(AST_LOG_DEBUG, "RDKAFKA-%i-%s: %s: %s\n", level, fac, rk ? rd_kafka_name(rk) : NULL, buf);
		break;
	}
}

static void destroy_routes(void)
{
	stasis_message_router_unsubscribe_and_join(stasis_router);
	stasis_router = NULL;
}

static void destroy_subscriptions(void)
{
	ao2_cleanup(aggregation_topic);
	aggregation_topic = NULL;

	channel_forwarder = stasis_forward_cancel(channel_forwarder);
	bridge_forwarder = stasis_forward_cancel(bridge_forwarder);
	parking_forwarder = stasis_forward_cancel(parking_forwarder);
	rtp_forwarder = stasis_forward_cancel(rtp_forwarder);
}

static int create_subscriptions(void)
{
	aggregation_topic = stasis_topic_create("kafka:aggregator");
	if (!aggregation_topic) {
		return -1;
	}

	channel_forwarder = stasis_forward_all(ast_channel_topic_all(), aggregation_topic);
	if (!channel_forwarder) {
		return -1;
	}

	bridge_forwarder = stasis_forward_all(ast_bridge_topic_all(), aggregation_topic);
	if (!bridge_forwarder) {
		return -1;
	}

	parking_forwarder = stasis_forward_all(ast_parking_topic(), aggregation_topic);
	if (!parking_forwarder) {
		return -1;
	}

	rtp_forwarder = stasis_forward_all(ast_rtp_topic(), aggregation_topic);
	if (!rtp_forwarder) {
		return -1;
	}

	return 0;
}

static int create_routes(void)
{
	int ret = 0;

	stasis_router = stasis_message_router_create(aggregation_topic);
	if (!stasis_router) {
		return -1;
	}

	stasis_message_router_set_congestion_limits(stasis_router, -1, 6 * AST_TASKPROCESSOR_HIGH_WATER_LEVEL);

	ret |= stasis_message_router_add(stasis_router, ast_channel_snapshot_type(), channel_snapshot_update_cb, NULL);
	ret |= stasis_message_router_add(stasis_router,
	                                 ast_channel_monitor_start_type(), channel_monitor_start_cb, NULL);
	ret |= stasis_message_router_add(stasis_router,
	                                 ast_channel_monitor_stop_type(), channel_monitor_stop_cb, NULL);
	ret |= stasis_message_router_add(stasis_router, ast_channel_varset_type(), ignore_cb, NULL);
	ret |= stasis_message_router_set_default(stasis_router, default_cb, NULL);

	if (ret) {
		ast_log(LOG_ERROR, "Failed to register for Stasis messages\n");
	}

	return ret;
}

static void kafka_producer_destructor(void *obj)
{
	struct kafka_producer *producer = obj;

	ast_debug(1, "Destroying Kafka producer '%s'\n", producer->name);

	if (producer->rk) {
		rd_kafka_flush(producer->rk, 2500); /* Wait for max 2.5 seconds */
		rd_kafka_destroy(producer->rk);
		producer->rk = NULL;
	}

	ast_free(producer->topic);
	producer->topic = NULL;
}

static int load_config(void)
{
	struct ast_config *cfg = NULL;
	struct ast_variable *var;
	struct ast_flags config_flags = {0};
	struct kafka_producer *new;
	rd_kafka_conf_t *rk_cfg = NULL;
	const char *cat;
	const char *topic;
	char err_str[512];

	cfg = ast_config_load(config_file, config_flags);

	if (cfg == CONFIG_STATUS_FILEMISSING) {
		ast_log(LOG_WARNING, "No such configuration file '%s'\n", config_file);
		return -1;
	} else if (cfg == CONFIG_STATUS_FILEINVALID) {
		ast_log(LOG_ERROR, "Config file '%s' is in an invalid format\n", config_file);
		return -1;
	}

	for (cat = ast_category_browse(cfg, NULL); cat; cat = ast_category_browse(cfg, cat)) {
		/* Setup common options from [general] section */
		if (!strcasecmp(cat, "general")) {
			for (var = ast_variable_browse(cfg, cat); var; var = var->next) {
				if (!strcasecmp(var->name, "enable")) {
					if (!ast_true(var->value)) {
						ast_log(LOG_NOTICE, "Module disabled\n");
						goto err;
					}
				}
			}
			continue;
		}

		if (!(rk_cfg = rd_kafka_conf_new())) {
			goto err;
		}
		for (var = ast_variable_browse(cfg, cat); var; var = var->next) {
			if (!strcasecmp(var->name, "topic")) {
				topic = var->value;
				continue;
			}

			if (rd_kafka_conf_set(rk_cfg, var->name, var->value, err_str, sizeof(err_str)) !=
			    RD_KAFKA_CONF_OK) {
				ast_log(LOG_ERROR, "[%s] %s\n", cat, err_str);
				goto err;
			}
		}

		if (ast_strlen_zero(topic)) {
			ast_log(LOG_ERROR, "[%s] Topic not specified\n", cat);
			goto err;
		}

		new = ao2_alloc(sizeof(*new), kafka_producer_destructor);
		if (!new) {
			goto err;
		}

		ast_copy_string(new->name, cat, sizeof(new->name));
		new->topic = ast_strdup(topic);
		rd_kafka_conf_set_log_cb(rk_cfg, rk_logger);
		if (!(new->rk = rd_kafka_new(RD_KAFKA_PRODUCER, rk_cfg, err_str, sizeof(err_str)))) {
			ast_log(LOG_ERROR, "[%s] Failed to create new producer: %s\n", cat, err_str);
			ao2_ref(new, -1);
			goto err;
		}

		ao2_link(kafka_producer_container, new);
		ao2_ref(new, -1);
		rk_cfg = NULL;
	}

	ast_config_destroy(cfg);

	return 0;

err:
	ast_config_destroy(cfg);
	if (rk_cfg)
		rd_kafka_conf_destroy(rk_cfg);

	return -1;
}

static struct ast_cli_entry cli_produce = AST_CLI_DEFINE(handle_cli_produce, "Produce message to Kafka");

static int unload_module(void)
{
	ast_cli_unregister(&cli_produce);
	destroy_routes();
	destroy_subscriptions();
	ao2_cleanup(kafka_producer_container);
	kafka_producer_container = NULL;

	return 0;
}

static int load_module(void)
{
	kafka_producer_container = ao2_container_alloc_list(AO2_ALLOC_OPT_LOCK_MUTEX, 0, NULL, NULL);
	if (!kafka_producer_container) {
		return AST_MODULE_LOAD_FAILURE;
	}

	if (load_config() || create_subscriptions() || create_routes()) {
		ao2_cleanup(kafka_producer_container);
		return AST_MODULE_LOAD_FAILURE;
	}

	if (ast_cli_register(&cli_produce)) {
		ao2_cleanup(kafka_producer_container);
		return AST_MODULE_LOAD_FAILURE;
	}

	return AST_MODULE_LOAD_SUCCESS;
}

static int reload(void)
{
	unload_module();
	return load_module();
}

//////@formatter:off
AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_DEFAULT, "Send stasis messages to Kafka",
    .support_level = AST_MODULE_SUPPORT_EXTENDED,
    .load = load_module,
    .unload = unload_module,
    .reload = reload,
);
